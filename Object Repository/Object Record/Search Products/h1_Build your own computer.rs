<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Build your own computer</name>
   <tag></tag>
   <elementGuidId>8994d7ff-b7b4-45d4-9be4-5487a368da86</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='product-details-form']/div/div/div[2]/div/h1</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>6fd12072-02ae-4daa-923e-015ee348b983</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>itemprop</name>
      <type>Main</type>
      <value>name</value>
      <webElementGuid>84691252-2504-43d6-809e-ebfea0e50157</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                Build your own computer
                            </value>
      <webElementGuid>967f6d3a-290a-4def-bf26-03209a866702</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;product-details-form&quot;)/div[1]/div[@class=&quot;product-essential&quot;]/div[@class=&quot;overview&quot;]/div[@class=&quot;product-name&quot;]/h1[1]</value>
      <webElementGuid>99934045-41f3-4fd7-bbe6-5c561d28ff54</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='product-details-form']/div/div/div[2]/div/h1</value>
      <webElementGuid>b5b81f63-0a8a-4ecc-b8c7-64c2802e2677</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Build your own computer'])[1]/following::h1[1]</value>
      <webElementGuid>e421b4af-cc7b-4146-a7fa-a9a7d52440c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='/'])[3]/following::h1[1]</value>
      <webElementGuid>298ddea7-fe87-47cd-b563-555f4ab1480f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Build it'])[1]/preceding::h1[1]</value>
      <webElementGuid>93f8a2df-127b-46d7-aac3-c2c2473701c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Availability:'])[1]/preceding::h1[1]</value>
      <webElementGuid>06625032-5f28-4cd2-8352-fb28b2ad5b92</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>b14d20d7-2bb0-4982-89a4-62b1023aa398</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = '
                                Build your own computer
                            ' or . = '
                                Build your own computer
                            ')]</value>
      <webElementGuid>e792bb45-b5dc-474d-914d-bcb2bd8be9f5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
