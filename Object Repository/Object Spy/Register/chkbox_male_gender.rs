<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>chkbox_male_gender</name>
   <tag></tag>
   <elementGuidId>63949f19-51f6-43e5-be3e-b391eabaf673</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#gender-male</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='gender-male']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>8c734684-f302-4624-aee4-0ae5609db7c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>gender-male</value>
      <webElementGuid>8dc6ea26-0151-47e9-8495-59573c3fc0ab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Gender</value>
      <webElementGuid>6f0cc036-1a16-4a01-84bc-e228362d3b32</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>radio</value>
      <webElementGuid>a5f05ed2-1998-4a8e-9c0d-5cb04f7de270</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>M</value>
      <webElementGuid>6f730c82-9ce6-4900-b420-e6ae33060f15</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;gender-male&quot;)</value>
      <webElementGuid>a564cfba-5825-40df-b9ed-565135dd7c0b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='gender-male']</value>
      <webElementGuid>1988d5c7-8541-4b9b-acd2-a30a7739ddd7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div/input</value>
      <webElementGuid>08225944-c16b-4b93-9f12-58f6cf309ed7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'gender-male' and @name = 'Gender' and @type = 'radio']</value>
      <webElementGuid>80069d64-2c3c-427d-996e-87d9ebe5814b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
