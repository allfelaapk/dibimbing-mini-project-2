<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_confirm_register</name>
   <tag></tag>
   <elementGuidId>8da409fa-6f7d-4253-8e47-1a698d7deaaf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.center-2</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[1]/following::div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>802241ac-f3c9-4529-a0af-8698ec3ab600</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>center-2</value>
      <webElementGuid>1e448dbe-2332-406d-ac12-274b0de40ffe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
    

    
        Register
    
    
        
            Your registration completed
        
        
            
        
    


    
</value>
      <webElementGuid>7298d158-4566-488e-a953-ae40a2ef058d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]</value>
      <webElementGuid>efbaee3b-df7f-41d8-b139-a79771507c47</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[1]/following::div[2]</value>
      <webElementGuid>75df5775-4d03-4835-8983-3a4fd5f34cd4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign up for our newsletter:'])[1]/following::div[4]</value>
      <webElementGuid>ac92742c-4d89-4197-8f01-d7a65bc4540a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div[4]/div[2]</value>
      <webElementGuid>299a12ad-1975-4efb-99be-499e52403941</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
    

    
        Register
    
    
        
            Your registration completed
        
        
            
        
    


    
' or . = '
    
    

    
        Register
    
    
        
            Your registration completed
        
        
            
        
    


    
')]</value>
      <webElementGuid>93c5d792-9f26-4cf3-aeb7-fc85de7d3327</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
