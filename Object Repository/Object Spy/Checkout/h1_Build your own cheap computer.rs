<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Build your own cheap computer</name>
   <tag></tag>
   <elementGuidId>0b06e89f-326b-4501-b235-b1f411d55cd1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='product-details-form']/div/div/div[2]/div/h1</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>5d35bf77-7b44-4ff9-809a-ba48aa1a51c2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>itemprop</name>
      <type>Main</type>
      <value>name</value>
      <webElementGuid>22a72c61-a01b-4b8d-9f0f-93195276af47</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                Build your own cheap computer
                            </value>
      <webElementGuid>cf64d533-bb9e-4905-8394-b1b7cf79be9c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;product-details-form&quot;)/div[1]/div[@class=&quot;product-essential&quot;]/div[@class=&quot;overview&quot;]/div[@class=&quot;product-name&quot;]/h1[1]</value>
      <webElementGuid>8ff4eee4-3a71-4080-aeea-ced7b41a5233</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='product-details-form']/div/div/div[2]/div/h1</value>
      <webElementGuid>25df9600-7d03-4993-9a55-4e2e3a6bcb79</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Build your own cheap computer'])[2]/following::h1[1]</value>
      <webElementGuid>c6fa3c78-fa80-4729-ae12-e2d5073cba3c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='/'])[3]/following::h1[1]</value>
      <webElementGuid>acec5533-eb8a-49be-9ea1-4968fa79a2b1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Build it'])[1]/preceding::h1[1]</value>
      <webElementGuid>7e1782ca-3d9b-42c5-8633-6a9e59bafe77</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Availability:'])[1]/preceding::h1[1]</value>
      <webElementGuid>c3c73f91-50b7-47b6-a5dc-229000db17bd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>c419b166-b5bd-4f1d-ba12-64a3b987168f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = '
                                Build your own cheap computer
                            ' or . = '
                                Build your own cheap computer
                            ')]</value>
      <webElementGuid>62c322c3-9d1c-49b2-bc06-4a715ed6ac14</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
