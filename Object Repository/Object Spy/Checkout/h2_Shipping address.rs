<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h2_Shipping address</name>
   <tag></tag>
   <elementGuidId>764b7c9c-547f-4569-a5a5-0ad3da29104c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='opc-shipping']/div/h2</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#opc-shipping > div.step-title > h2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>7d973c9d-4d7b-4864-ad18-948ac3cd86fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Shipping address</value>
      <webElementGuid>4fa96926-e6c3-42ce-97ab-19e72ed7d524</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;opc-shipping&quot;)/div[@class=&quot;step-title&quot;]/h2[1]</value>
      <webElementGuid>8a6ea3ae-6601-4425-aa9e-015d850d8e4b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='opc-shipping']/div/h2</value>
      <webElementGuid>e949d574-1ab7-4b7e-8227-84054716255f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading next step...'])[1]/following::h2[1]</value>
      <webElementGuid>d3a27c4f-1a6c-4a44-bb4b-77234b2d517f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fax number:'])[1]/following::h2[1]</value>
      <webElementGuid>828f81cb-2831-44ad-a377-76bc49c7c00b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select a shipping address from your address book or enter a new address.'])[1]/preceding::h2[1]</value>
      <webElementGuid>8c9501a6-ec79-4e60-9db4-b371880d533e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='First name:'])[2]/preceding::h2[1]</value>
      <webElementGuid>d0fa1437-2d3b-49d2-a8d8-f33ba14771c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Shipping address']/parent::*</value>
      <webElementGuid>e429122f-0fe4-4731-a6ed-88d7c9376aa4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/div/h2</value>
      <webElementGuid>0120236d-de4d-4680-a0e6-3a81bf8e4006</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[(text() = 'Shipping address' or . = 'Shipping address')]</value>
      <webElementGuid>cc87bfd9-717f-426f-a51b-ee7414faa2e0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
