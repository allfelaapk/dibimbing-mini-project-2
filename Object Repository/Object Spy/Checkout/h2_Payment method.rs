<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h2_Payment method</name>
   <tag></tag>
   <elementGuidId>ddeaaaf6-d669-44d2-aad5-104b26edf0fe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='opc-payment_method']/div/h2</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#opc-payment_method > div.step-title > h2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>f2d1f68f-624b-49bb-9936-f34a2821f9b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Payment method</value>
      <webElementGuid>fe5ee916-f4b3-4b5f-910a-2a349d0d47ee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;opc-payment_method&quot;)/div[@class=&quot;step-title&quot;]/h2[1]</value>
      <webElementGuid>ef98155f-f817-4064-bf2d-9c99b1a29b02</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='opc-payment_method']/div/h2</value>
      <webElementGuid>95bc454e-fcf0-44d2-a570-69ce9c5adb1f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading next step...'])[3]/following::h2[1]</value>
      <webElementGuid>6d54dac2-6671-4a10-833c-33e3b01087f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='«'])[2]/following::h2[1]</value>
      <webElementGuid>2f400ec3-e8cb-4305-8a51-0ddf3b4a20de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cash On Delivery (COD) (7.00)'])[1]/preceding::h2[1]</value>
      <webElementGuid>03801ff0-0c2c-4fb6-8b0e-64bbc0837912</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Check / Money Order (5.00)'])[1]/preceding::h2[1]</value>
      <webElementGuid>7bab6d62-ddcf-4f09-acf1-ce2991d205f6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Payment method']/parent::*</value>
      <webElementGuid>f1747f63-091c-4811-a744-f9858d66f387</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[4]/div/h2</value>
      <webElementGuid>d87c53f0-f313-44b2-b75b-3fc15046ac7a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[(text() = 'Payment method' or . = 'Payment method')]</value>
      <webElementGuid>58adce99-c715-4158-9202-fbee95264608</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
