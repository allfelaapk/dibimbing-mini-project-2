<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Billing Address                        _fdc7db</name>
   <tag></tag>
   <elementGuidId>5a075773-b130-442c-836d-f960af044a9c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-confirm-order-load']/div/div[2]/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.order-review-data</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>253ec90a-6ac5-4564-bbea-b5d0c378d728</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>order-review-data</value>
      <webElementGuid>40e5df90-0e93-4d90-8508-3f6b848ad2d3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        
            
                Billing Address
            
            
                bayu test
            
            
                Email: bayutest@gmail.com
            
                
                    Phone: 08189278341
                
                            
                    Fax: 
                
                                        
                    Jl. Bulak
                
                                        
Surabaya                                            ,
                                        78192                
                            
                    Indonesia
                
                            
                        Payment Method
                
                
                        Check / Money Order
                
        
            
                    
                        
                            Shipping Address
                    
                    
                        bayu test
                    
                    
                        Email: bayutest@gmail.com
                    
                        
                            Phone: 08189278341
                        
                        
                            Fax: 
                        
                        
                            Jl. Bulak
                        
                        
Surabaya                                                            ,
                                                        78192                        
                        
                            Indonesia
                        
                
                    Shipping Method
                
                
                    Next Day Air
                
            
    </value>
      <webElementGuid>c37f0d51-605a-4573-b2aa-6641f27c3078</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-confirm-order-load&quot;)/div[@class=&quot;checkout-data&quot;]/div[@class=&quot;order-summary-body&quot;]/div[@class=&quot;order-summary-content&quot;]/div[@class=&quot;order-review-data&quot;]</value>
      <webElementGuid>e89a83c4-8c35-4c7e-ae20-f4eb9ed48e67</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-confirm-order-load']/div/div[2]/div/div</value>
      <webElementGuid>ff31eda1-7e8f-4f9a-9b93-be82cfb9bcc0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Confirm order'])[1]/following::div[8]</value>
      <webElementGuid>565857f8-66fb-4192-8437-cd797267133f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading next step...'])[5]/following::div[9]</value>
      <webElementGuid>2f2192c9-ec47-4def-8bfd-e5c2e0fb7013</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[6]/div[2]/div/div/div[2]/div/div</value>
      <webElementGuid>1c9d760c-215a-462a-8304-bba25b3a07fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        
            
                Billing Address
            
            
                bayu test
            
            
                Email: bayutest@gmail.com
            
                
                    Phone: 08189278341
                
                            
                    Fax: 
                
                                        
                    Jl. Bulak
                
                                        
Surabaya                                            ,
                                        78192                
                            
                    Indonesia
                
                            
                        Payment Method
                
                
                        Check / Money Order
                
        
            
                    
                        
                            Shipping Address
                    
                    
                        bayu test
                    
                    
                        Email: bayutest@gmail.com
                    
                        
                            Phone: 08189278341
                        
                        
                            Fax: 
                        
                        
                            Jl. Bulak
                        
                        
Surabaya                                                            ,
                                                        78192                        
                        
                            Indonesia
                        
                
                    Shipping Method
                
                
                    Next Day Air
                
            
    ' or . = '
        
            
                Billing Address
            
            
                bayu test
            
            
                Email: bayutest@gmail.com
            
                
                    Phone: 08189278341
                
                            
                    Fax: 
                
                                        
                    Jl. Bulak
                
                                        
Surabaya                                            ,
                                        78192                
                            
                    Indonesia
                
                            
                        Payment Method
                
                
                        Check / Money Order
                
        
            
                    
                        
                            Shipping Address
                    
                    
                        bayu test
                    
                    
                        Email: bayutest@gmail.com
                    
                        
                            Phone: 08189278341
                        
                        
                            Fax: 
                        
                        
                            Jl. Bulak
                        
                        
Surabaya                                                            ,
                                                        78192                        
                        
                            Indonesia
                        
                
                    Shipping Method
                
                
                    Next Day Air
                
            
    ')]</value>
      <webElementGuid>9c7722b0-b96d-46de-a176-7ecad711b6ff</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
