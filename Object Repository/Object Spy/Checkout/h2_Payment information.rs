<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h2_Payment information</name>
   <tag></tag>
   <elementGuidId>8fa5bd51-db33-4d64-9a00-84fa42d904e4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='opc-payment_info']/div/h2</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#opc-payment_info > div.step-title > h2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>96df0b96-a029-4635-9eef-7ab8fba10908</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Payment information</value>
      <webElementGuid>7db79386-fcf2-455d-9adb-9df4468f7f39</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;opc-payment_info&quot;)/div[@class=&quot;step-title&quot;]/h2[1]</value>
      <webElementGuid>d1c31206-9979-4712-bb80-b7ad4e3a8a2e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='opc-payment_info']/div/h2</value>
      <webElementGuid>66b62cf4-60f3-4416-a547-76e4fc13846f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading next step...'])[4]/following::h2[1]</value>
      <webElementGuid>fc82ca70-7752-40a9-a03a-c75658434a72</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='«'])[3]/following::h2[1]</value>
      <webElementGuid>3d1d0453-9bda-4c45-982f-4f64dae3a189</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tricentis GmbH'])[1]/preceding::h2[1]</value>
      <webElementGuid>3c477a43-c136-444f-8e12-a2ca1a4f9be8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Leonard-Bernstein-Straße 10'])[1]/preceding::h2[1]</value>
      <webElementGuid>660091fa-bf18-4213-9532-51a711fa36d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Payment information']/parent::*</value>
      <webElementGuid>28eba7c9-5488-4557-b94a-4da8bb0a2a21</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[5]/div/h2</value>
      <webElementGuid>bc1969da-8122-4a70-a719-331d76dd25e4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[(text() = 'Payment information' or . = 'Payment information')]</value>
      <webElementGuid>4395bb9f-e7f3-4541-9096-01c3f12245dc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
