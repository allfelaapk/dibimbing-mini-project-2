<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_Mail Personal or Business Check, Cashier_d5c3d6</name>
   <tag></tag>
   <elementGuidId>509f434b-5869-4386-a783-dedc709575fe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-payment-info-load']/div/div/div/table/tbody/tr/td</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>td</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>78fc2fef-0cf1-4733-8b2a-c4fc471ee98f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Mail Personal or Business Check, Cashier's Check or money order to:Tricentis GmbH Leonard-Bernstein-Straße 10 1220 Vienna AustriaNotice that if you pay by Personal or Business Check, your order may be held for up to 10 days after we receive your check to allow enough time for the check to clear.  If you want us to ship faster upon receipt of your payment, then we recommend your send a money order or Cashier's check.P.S. You can edit this text from admin panel.
        </value>
      <webElementGuid>1ff2e7fe-d4f1-4d02-a684-02f85a33abac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-payment-info-load&quot;)/div[@class=&quot;checkout-data&quot;]/div[@class=&quot;section payment-info&quot;]/div[@class=&quot;info&quot;]/table[1]/tbody[1]/tr[1]/td[1]</value>
      <webElementGuid>922f3296-c535-49ee-a0a2-3d56a75930e3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-payment-info-load']/div/div/div/table/tbody/tr/td</value>
      <webElementGuid>aed02d3f-f75d-4096-9c2d-7efae2770c71</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payment information'])[1]/following::td[1]</value>
      <webElementGuid>c3f59e9c-8918-4c78-ae19-17679ec2e345</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading next step...'])[4]/following::td[1]</value>
      <webElementGuid>61696963-7d72-4ea4-96f1-b48725fca3b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td</value>
      <webElementGuid>7936f876-9f84-4100-85b9-474d657c9801</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = concat(&quot;
            Mail Personal or Business Check, Cashier&quot; , &quot;'&quot; , &quot;s Check or money order to:Tricentis GmbH Leonard-Bernstein-Straße 10 1220 Vienna AustriaNotice that if you pay by Personal or Business Check, your order may be held for up to 10 days after we receive your check to allow enough time for the check to clear.  If you want us to ship faster upon receipt of your payment, then we recommend your send a money order or Cashier&quot; , &quot;'&quot; , &quot;s check.P.S. You can edit this text from admin panel.
        &quot;) or . = concat(&quot;
            Mail Personal or Business Check, Cashier&quot; , &quot;'&quot; , &quot;s Check or money order to:Tricentis GmbH Leonard-Bernstein-Straße 10 1220 Vienna AustriaNotice that if you pay by Personal or Business Check, your order may be held for up to 10 days after we receive your check to allow enough time for the check to clear.  If you want us to ship faster upon receipt of your payment, then we recommend your send a money order or Cashier&quot; , &quot;'&quot; , &quot;s check.P.S. You can edit this text from admin panel.
        &quot;))]</value>
      <webElementGuid>2f2834a0-1915-4b61-9ae0-caafdb224dde</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
