<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Click here for order details</name>
   <tag></tag>
   <elementGuidId>746eca58-4ee8-459f-9d35-122b5103990b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Click here for order details.')]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>008626d7-3111-4857-b560-ab059723c2f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/orderdetails/1630322</value>
      <webElementGuid>6e08d473-a93e-4e80-a6e8-efe5576e056d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Click here for order details.</value>
      <webElementGuid>696ed0ac-f545-47e7-ab15-dbaaddfb939a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-1&quot;]/div[@class=&quot;page checkout-page&quot;]/div[@class=&quot;page-body checkout-data&quot;]/div[@class=&quot;section order-completed&quot;]/ul[@class=&quot;details&quot;]/li[2]/a[1]</value>
      <webElementGuid>20819c02-0ccc-4ab4-ac9d-b4371f5cc80f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Click here for order details.')]</value>
      <webElementGuid>f6e1e026-43f0-40bb-9762-16f2278ac93f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Order number: 1630322'])[1]/following::a[1]</value>
      <webElementGuid>50b4c192-7ec5-48e2-8cbf-b330b803a211</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Your order has been successfully processed!'])[1]/following::a[1]</value>
      <webElementGuid>73d5eef2-7f25-4f5e-b6b5-baad01ccf0a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information'])[1]/preceding::a[1]</value>
      <webElementGuid>a86e6be9-7239-4f23-8461-17eed49f3524</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sitemap'])[1]/preceding::a[1]</value>
      <webElementGuid>719379ff-7bbf-4d84-a1fb-78f562027f36</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Click here for order details.']/parent::*</value>
      <webElementGuid>12ea31ea-6e50-4a46-b8d3-508d8cc42b67</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/orderdetails/1630322')]</value>
      <webElementGuid>11d1c9d0-d0e9-49ee-942d-a7dccb0b21b1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[4]/div/div/div[2]/div/ul/li[2]/a</value>
      <webElementGuid>ec0e768d-9f84-4314-9256-231d2de04d92</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/orderdetails/1630322' and (text() = 'Click here for order details.' or . = 'Click here for order details.')]</value>
      <webElementGuid>d56f9132-7a51-41b1-b942-d113e02dec24</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
