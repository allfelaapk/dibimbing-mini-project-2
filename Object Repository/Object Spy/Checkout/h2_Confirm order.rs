<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h2_Confirm order</name>
   <tag></tag>
   <elementGuidId>6934db6f-babf-4e23-99ec-b26e76e06cf9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='opc-confirm_order']/div/h2</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#opc-confirm_order > div.step-title > h2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>598316c0-6947-43ef-8ab3-de49ab57437a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Confirm order</value>
      <webElementGuid>b56103af-21ec-4869-8a06-e69f107ee5cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;opc-confirm_order&quot;)/div[@class=&quot;step-title&quot;]/h2[1]</value>
      <webElementGuid>b028cb0b-d1d5-4a06-ba6f-9c93ac706576</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='opc-confirm_order']/div/h2</value>
      <webElementGuid>41a57705-87b3-40f2-bddc-dae83b9cf198</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading next step...'])[5]/following::h2[1]</value>
      <webElementGuid>baf7d1e0-d6b5-4c3b-8391-41f89e2313c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='«'])[4]/following::h2[1]</value>
      <webElementGuid>10f71591-85d1-4e1e-901b-a5840df00a47</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Billing Address'])[1]/preceding::h2[1]</value>
      <webElementGuid>4e9fc25e-ce08-47f3-98d0-e5b752e9052b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='bayu test'])[1]/preceding::h2[1]</value>
      <webElementGuid>72c05a9f-83c4-45f0-8e2b-10a81e4bdaf9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Confirm order']/parent::*</value>
      <webElementGuid>575bd5b6-587b-4bfe-a814-cdcde968fe39</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[6]/div/h2</value>
      <webElementGuid>5633bd06-dece-4139-8d23-85431a7bbe3c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[(text() = 'Confirm order' or . = 'Confirm order')]</value>
      <webElementGuid>e670c093-5db4-43bd-a87c-b69c4e7ae56d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
