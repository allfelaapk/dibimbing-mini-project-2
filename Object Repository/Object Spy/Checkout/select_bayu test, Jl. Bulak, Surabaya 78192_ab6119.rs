<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_bayu test, Jl. Bulak, Surabaya 78192_ab6119</name>
   <tag></tag>
   <elementGuidId>c7835794-8888-41f1-8168-6a3e1049c07f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='billing-address-select']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#billing-address-select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>40896200-1d8f-43f0-a0ae-15c467e078ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>billing_address_id</value>
      <webElementGuid>afb2fca9-f217-403e-a709-8a71a65736ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>billing-address-select</value>
      <webElementGuid>9a8fc7e1-b749-4182-8890-79ceef58c95c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>address-select</value>
      <webElementGuid>0b4872d4-997c-4de0-8443-8f7a01c1c52f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>Billing.newAddress(!this.value)</value>
      <webElementGuid>7cb425cc-b5fd-403d-97d5-3a76382a2a34</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        bayu test, Jl. Bulak, Surabaya 78192, Indonesia
                    New Address
                </value>
      <webElementGuid>2c86b1fd-f8ec-40d6-87eb-3a1837a2a2fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;billing-address-select&quot;)</value>
      <webElementGuid>14775e5c-0e2a-4a16-8c1f-abbfe726e7ab</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='billing-address-select']</value>
      <webElementGuid>1557e7da-7f7b-4dd6-a46a-bb22156345bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-billing-load']/div/div/div/select</value>
      <webElementGuid>19b1b9a6-13e0-449b-bdaf-61c9666073d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select a billing address from your address book or enter a new address.'])[1]/following::select[1]</value>
      <webElementGuid>c60ef93c-8dc5-4016-b2c7-fba52235d1f6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Billing address'])[1]/following::select[1]</value>
      <webElementGuid>d71b6700-5e65-40de-9456-d33eee12364a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='First name:'])[1]/preceding::select[1]</value>
      <webElementGuid>43c4ccf8-b11d-40f4-a43c-e2a819b61690</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[1]/preceding::select[1]</value>
      <webElementGuid>d10c6eb5-bb5d-4ad7-8c05-ec954549b1a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>96bc854e-cd9a-45ec-8a46-711036930adb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'billing_address_id' and @id = 'billing-address-select' and (text() = '
                        bayu test, Jl. Bulak, Surabaya 78192, Indonesia
                    New Address
                ' or . = '
                        bayu test, Jl. Bulak, Surabaya 78192, Indonesia
                    New Address
                ')]</value>
      <webElementGuid>639f544e-c74a-46de-9887-c7573693b576</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
