<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h2_Shipping method</name>
   <tag></tag>
   <elementGuidId>d510bcf3-4e55-4fee-b930-203e6d0717d2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='opc-shipping_method']/div/h2</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#opc-shipping_method > div.step-title > h2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>9dd924a8-7bcc-4bb2-81e3-c1e72d9d9b98</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Shipping method</value>
      <webElementGuid>c2839d85-2710-421c-bb74-8f6cc3160403</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;opc-shipping_method&quot;)/div[@class=&quot;step-title&quot;]/h2[1]</value>
      <webElementGuid>5c03d77c-7750-4ea5-be30-230a7715a30f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='opc-shipping_method']/div/h2</value>
      <webElementGuid>5765183c-a690-4cfb-a5d0-aaef67d4bfa9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading next step...'])[2]/following::h2[1]</value>
      <webElementGuid>c7435927-d580-4e00-96e6-3eb0b5ba3eaa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='«'])[1]/following::h2[1]</value>
      <webElementGuid>ddf61a1c-08ca-4db3-92e9-e455daad5913</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ground (0.00)'])[1]/preceding::h2[1]</value>
      <webElementGuid>16afc1ea-f1d6-4262-9639-4fa2ace26615</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next Day Air (0.00)'])[1]/preceding::h2[1]</value>
      <webElementGuid>3355cd58-1427-4e41-96b7-81e85f3fc238</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Shipping method']/parent::*</value>
      <webElementGuid>66b49ff6-394f-4bb6-9817-575cd28bf118</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/div/h2</value>
      <webElementGuid>a42c7d39-2202-491f-a796-d4ab3ec791c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[(text() = 'Shipping method' or . = 'Shipping method')]</value>
      <webElementGuid>0d4a1cbb-a0c5-4215-ad3b-db88b691a5ba</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
