<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_sign in page</name>
   <tag></tag>
   <elementGuidId>4b25ccbc-4fdd-4745-952c-e98c5cf7875b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.center-2</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[1]/following::div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>8e70219e-ec90-4f11-a25c-44f50f92dd96</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>center-2</value>
      <webElementGuid>cfd50821-02e3-4d72-b279-06ac1a955714</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
    

    
        Welcome, Please Sign In!
    
    

    

    
        
                
                    
                        New Customer
                    
                    
                        By creating an account on our website you will be able to shop faster, be up to date on an orders status, and keep track of the orders you have previously made.
                    
                    
                        
                    
                
            
                
                    Returning Customer
                
                
                        
                            
                        
                        
                            Email:
                            
                            
                        
                        
                            Password:
                            
                            
                        
                        
                            
                            Remember me?
                            
                                Forgot password?
                            
                        
                        
                            
                        
                
            
        
        
            
        
            
            
                
                    About login / registration
            
        
            Put your login / registration information here. You can edit this in the admin site.
        
    

    


    
</value>
      <webElementGuid>b9abb906-3f67-4d85-a6fe-f8e8e7de0691</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]</value>
      <webElementGuid>9e291df0-ba84-428d-8e87-1e7666dfc6fb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[1]/following::div[2]</value>
      <webElementGuid>eee60716-65e3-4bcb-8dfa-c137a1e621a1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign up for our newsletter:'])[1]/following::div[4]</value>
      <webElementGuid>a9752532-04ad-4608-971c-6c98eb79eff4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div[4]/div[2]</value>
      <webElementGuid>00af38ec-d154-431a-8ea8-d0d2e2b16404</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
    

    
        Welcome, Please Sign In!
    
    

    

    
        
                
                    
                        New Customer
                    
                    
                        By creating an account on our website you will be able to shop faster, be up to date on an orders status, and keep track of the orders you have previously made.
                    
                    
                        
                    
                
            
                
                    Returning Customer
                
                
                        
                            
                        
                        
                            Email:
                            
                            
                        
                        
                            Password:
                            
                            
                        
                        
                            
                            Remember me?
                            
                                Forgot password?
                            
                        
                        
                            
                        
                
            
        
        
            
        
            
            
                
                    About login / registration
            
        
            Put your login / registration information here. You can edit this in the admin site.
        
    

    


    
' or . = '
    
    

    
        Welcome, Please Sign In!
    
    

    

    
        
                
                    
                        New Customer
                    
                    
                        By creating an account on our website you will be able to shop faster, be up to date on an orders status, and keep track of the orders you have previously made.
                    
                    
                        
                    
                
            
                
                    Returning Customer
                
                
                        
                            
                        
                        
                            Email:
                            
                            
                        
                        
                            Password:
                            
                            
                        
                        
                            
                            Remember me?
                            
                                Forgot password?
                            
                        
                        
                            
                        
                
            
        
        
            
        
            
            
                
                    About login / registration
            
        
            Put your login / registration information here. You can edit this in the admin site.
        
    

    


    
')]</value>
      <webElementGuid>bad779f1-7f18-4a88-84bb-f395817ecc6f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
