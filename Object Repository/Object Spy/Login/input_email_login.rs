<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_email_login</name>
   <tag></tag>
   <elementGuidId>db8dd527-4c04-4e59-8ba2-af8c1aa58877</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#Email</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='Email']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>5333efb7-005c-42d8-a3df-ec738b9d285e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autofocus</name>
      <type>Main</type>
      <value>autofocus</value>
      <webElementGuid>40595996-b943-442c-be99-f5031a4880f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>b544e442-5959-43c9-b90c-560f4c417c43</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Email</value>
      <webElementGuid>4fcc037a-8933-42aa-9ff8-cdf388f572a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Email</value>
      <webElementGuid>0cb974ff-54af-45f7-8d21-12b152684a51</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>9bb7a4ac-53a4-4c3c-968d-a79c00eef4ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Email&quot;)</value>
      <webElementGuid>fcc1994c-70a6-4fdc-b5cf-5e1ab940118e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='Email']</value>
      <webElementGuid>a6b2177c-2c29-417f-a64b-18d1f41dc136</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div[2]/input</value>
      <webElementGuid>39dc895c-b75e-4f55-b2ad-f378ed881904</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'Email' and @name = 'Email' and @type = 'text']</value>
      <webElementGuid>71c331e4-86aa-4a1c-82d7-37512b6e5ed9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
