<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h2_welcoming page</name>
   <tag></tag>
   <elementGuidId>350f1012-a467-4a08-ab8c-aafb4f5d3c7a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Next'])[1]/following::h2[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h2.topic-html-content-header</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>a3632cbf-5869-4be5-a581-b167e0aca57b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>topic-html-content-header</value>
      <webElementGuid>de496e0b-a660-49b8-bc98-b93a0f126774</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    Welcome to our store</value>
      <webElementGuid>d06dd3cb-fae6-4ffa-8ae1-cadbce506f9d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-3&quot;]/div[@class=&quot;page home-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;topic-html-content&quot;]/div[@class=&quot;topic-html-content-title&quot;]/h2[@class=&quot;topic-html-content-header&quot;]</value>
      <webElementGuid>dc7e2484-59a4-4753-ab99-1c84c6358b81</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next'])[1]/following::h2[1]</value>
      <webElementGuid>e575954d-4c42-4dbf-9706-ddffbd2744cd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Prev'])[1]/following::h2[1]</value>
      <webElementGuid>a4a9ee39-14ab-4569-84e7-aa8392dac642</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Featured products'])[1]/preceding::h2[1]</value>
      <webElementGuid>06f1f66e-35db-457a-a3b3-ee222041138b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$25 Virtual Gift Card'])[1]/preceding::h2[1]</value>
      <webElementGuid>5feb2b44-f153-4c28-9828-ed5755e4e4dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Welcome to our store']/parent::*</value>
      <webElementGuid>b6c97fec-3220-4384-9179-597ca3c6159f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h2</value>
      <webElementGuid>0ab2d258-4b5a-46c0-8411-845c8338d977</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[(text() = '
                    Welcome to our store' or . = '
                    Welcome to our store')]</value>
      <webElementGuid>47fd674d-f032-4162-ba2a-f3e2b08a0e36</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
