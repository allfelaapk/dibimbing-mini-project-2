<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_add to cart-1</name>
   <tag></tag>
   <elementGuidId>a63e58aa-5bcd-45b3-9a45-7d4485d0ab65</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//input[@value='Add to cart'])[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>13d97ca4-fd41-4a7a-af7b-b6c393989e23</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>e69d0ebf-d20d-49fb-9544-7163e2d5cf9f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Add to cart</value>
      <webElementGuid>ced3b037-c747-4bbd-baf1-6c56f6765c38</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button-2 product-box-add-to-cart-button</value>
      <webElementGuid>522d96f2-ceac-4a2f-9bbe-7f7f18f734f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>AjaxCart.addproducttocart_catalog('/addproducttocart/catalog/72/1/1    ');return false;</value>
      <webElementGuid>57986add-b5dc-4970-b0cf-9680f81188ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-3&quot;]/div[@class=&quot;page home-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;product-grid home-page-product-grid&quot;]/div[@class=&quot;item-box&quot;]/div[@class=&quot;product-item&quot;]/div[@class=&quot;details&quot;]/div[@class=&quot;add-info&quot;]/div[@class=&quot;buttons&quot;]/input[@class=&quot;button-2 product-box-add-to-cart-button&quot;]</value>
      <webElementGuid>346e5a35-3cde-4d1a-8c8b-1251d3981b2e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@value='Add to cart'])[3]</value>
      <webElementGuid>d06a925e-ee27-45af-ba6a-8132355161d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div[2]/div[3]/div[2]/input</value>
      <webElementGuid>c17eb150-5d32-4306-9407-aa7549d27d19</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'button']</value>
      <webElementGuid>6c40497b-c8e2-4527-a03b-addcd067c608</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
