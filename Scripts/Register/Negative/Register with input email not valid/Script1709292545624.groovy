import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.BASE_URL)

WebUI.click(findTestObject('Object Spy/Navbar/navbar_register'))

WebUI.waitForElementVisible(findTestObject('Object Spy/Register/h1_Register'), 0)

/*WebUI.verifyElementVisible(findTestObject('Object Spy/Page_Demo Web Shop. Register/h1_Register'), FailureHandling.STOP_ON_FAILURE)*/
WebUI.click(findTestObject('Object Spy/Register/chkbox_male_gender'))

WebUI.setText(findTestObject('Object Spy/Register/input_firstname'), firstname)

WebUI.setText(findTestObject('Object Spy/Register/input_lastname'), lastname)

WebUI.setText(findTestObject('Object Spy/Register/input_email'), email)

WebUI.setEncryptedText(findTestObject('Object Spy/Register/input_password'), '3h7z7303dyFM79u3INqtyw==')

WebUI.setEncryptedText(findTestObject('Object Spy/Register/input_confirmpassword'), '3h7z7303dyFM79u3INqtyw==')

WebUI.click(findTestObject('Object Spy/Register/btn_register'))

WebUI.verifyElementPresent(findTestObject('Object Spy/Register/span_Wrong email'), 0)

