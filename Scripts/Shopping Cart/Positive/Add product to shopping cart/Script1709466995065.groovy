import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Reusable test case/Login/Login with valid credential'), [('email') : 'bayutest@gmail.com'
        , ('password') : 'test123654'], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_Demo Web Shop. Checkout/btn_add to cart-1'))

WebUI.waitForElementPresent(findTestObject('Page_Demo Web Shop. Checkout/h1_Build your own cheap computer'), 0)

WebUI.verifyElementPresent(findTestObject('Page_Demo Web Shop. Checkout/h1_Build your own cheap computer'), 0)

WebUI.click(findTestObject('Page_Demo Web Shop. Checkout/btn_add to cart-2'))

WebUI.click(findTestObject('Object Spy/Navbar/navbar_Shopping cart'))

WebUI.click(findTestObject('Page_Demo Web Shop. Shopping Cart/btn_estimateshipping'))

WebUI.click(findTestObject('Page_Demo Web Shop. Shopping Cart/chk_tnc'))

WebUI.click(findTestObject('Page_Demo Web Shop. Shopping Cart/btn_checkout'))

WebUI.verifyElementPresent(findTestObject('Page_Demo Web Shop. Checkout/h1_Checkout'), 0)

